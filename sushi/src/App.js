import React, {Component} from 'react';
import Navbar from './components/navbar';
import Card from './components/card';




class App extends Component {

  constructor() {
    super();
    this.state = { cards: [] };
  }

  async getCategories() {
    const response = await fetch("http://reactsushi.com");
    const data = await response.json();
    this.setState({ cards: data });
  }

  componentDidMount() {
    this.getCategories();
  }

  handleDelete = cardID => {
    const cards = this.state.cards.filter(card => card.id !== cardID);
    this.setState({cards});
  }

  handleIncrement = card => {
    const cards = [...this.state.cards];
    const id = cards.indexOf(card);
    cards[id] =  { ...card};
    cards[id].quantità++;
    this.setState({cards});
  }

  render(){
    return (
      <>
        <Navbar />
        <div className='container' style={{textAlign: 'center'}}>
          <h1>Cosa desideri ordinare?</h1>
          
          <div className='row text-center'>
            {this.state.cards.map(card =>(
                <Card 
                  key = {card.id}
                  onDelete = {this.handleDelete}
                  onIncrement = {this.handleIncrement}
                  card = {card}
                />
                
              ))}            
          </div>
        </div>
        
      </>
    );
  }
  
}

export default App;
